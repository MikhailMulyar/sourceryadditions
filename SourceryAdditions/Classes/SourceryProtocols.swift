//
// Created by Mikhail Mulyar on 25/07/2017.
// Copyright (c) 2017 Mikhail Mulyar. All rights reserved.
//

import Foundation


public protocol AutoEquatable {
}


public protocol AutoHashable {
}


public protocol AutoCases {
}


public protocol AutoCodable: Codable {
}


public protocol AutoInitializable {
}


public protocol AutoLenses {
}


public protocol AutoObjectDiff {
}


public protocol AutoValidation {
}


public protocol AutoSwinjectableService {
}


public protocol AutoSwinjectableModule {
}


