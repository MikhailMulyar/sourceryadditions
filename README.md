# SourceryAdditions

[![CI Status](http://img.shields.io/travis/mikhailmulyar/SourceryAdditions.svg?style=flat)](https://travis-ci.org/mikhailmulyar/SourceryAdditions)
[![Version](https://img.shields.io/cocoapods/v/SourceryAdditions.svg?style=flat)](http://cocoapods.org/pods/SourceryAdditions)
[![License](https://img.shields.io/cocoapods/l/SourceryAdditions.svg?style=flat)](http://cocoapods.org/pods/SourceryAdditions)
[![Platform](https://img.shields.io/cocoapods/p/SourceryAdditions.svg?style=flat)](http://cocoapods.org/pods/SourceryAdditions)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SourceryAdditions is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SourceryAdditions'
```

## Author

mikhailmulyar, mulyarm@gmail.com

## License

SourceryAdditions is available under the MIT license. See the LICENSE file for more info.
